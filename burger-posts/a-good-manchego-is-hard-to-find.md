---
title: A Good Manchego is Hard to Find
date: "2022-01-25"
tags: ["cheeseburger", "onion", "savory"]
draft: false
summary: A beautiful manchego, carmelized shallot, and fig jam beauty.
images: ["003_a-good-manchego-is-hard-to-find.png"]
authors: ["steve"]
layout: DEFAULT
---

Sir Robert Belcher touched God when he merged carmelized shallots with manchego
and fig jam on top of juicy juicy meat. 

Make this sando.

