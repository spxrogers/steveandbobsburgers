/*
 * Original file from:
 * https://raw.githubusercontent.com/timlrx/tailwind-nextjs-starter-blog/master/scripts/compose.js
 */
// eslint-disable-next-line @typescript-eslint/no-var-requires
const fs = require("fs");
// eslint-disable-next-line @typescript-eslint/no-var-requires
const inquirer = require("inquirer");
// eslint-disable-next-line @typescript-eslint/no-var-requires
const dedent = require("dedent");

/*
 * https://gohugo.io/content-management/front-matter/
 */
const genFrontMatter = (answers, fileName) =>
  dedent`---
  title: ${answers.title}
  date: "${answers.postDate}"
  tags: [${answers.tags
    .split(",")
    .map((tag) => tag.trim())
    .filter((tag) => tag.length > 0)
    .map((tag) => `"${tag}"`)
    .join(", ")}]
  draft: ${answers.draft === "yes"}
  summary: ${answers.summary ? answers.summary : " "}
  images: ["0XX_${fileName}"] # TODO: complete after committing the image.
  authors: ["steve"]
  layout: DEFAULT
  ---
  `;

inquirer
  .prompt([
    {
      name: "title",
      message: "Enter post title:",
      type: "input",
      validate: (title) => title && title.length > 0,
    },
    {
      name: "postDate",
      message: "What date is this posted? (YYYY-MM-DD)",
      type: "input",
      validate: (date) => date && date.length === 10,
    },
    {
      name: "summary",
      message: "Enter post summary:",
      type: "input",
    },
    {
      name: "draft",
      message: "Set post as draft?",
      type: "list",
      choices: ["yes", "no"],
    },
    {
      name: "tags",
      message: "Any Tags? Separate them with , or leave empty if no tags.",
      type: "input",
    },
  ])
  .then((answers) => {
    // Remove special characters and replace space with -
    const fileName = answers.title
      .toLowerCase()
      .replace(/[^a-zA-Z0-9 ]/g, "")
      .replace(/ /g, "-")
      .replace(/-+/g, "-");

    if (!fileName) {
      throw `Invalid file name generated from title: '${answers.title}'`;
    }

    const frontMatter = genFrontMatter(answers, fileName);

    const filePath = `burger-posts/${fileName}.md`;
    fs.writeFile(filePath, frontMatter, { flag: "wx" }, (err) => {
      if (err) {
        throw err;
      } else {
        console.log(`Burger post generated successfully at ${filePath}`);
      }
    });
  })
  .catch((error) => {
    if (error.isTtyError) {
      console.log("Prompt couldn't be rendered in the current environment");
    } else {
      console.log(error);
      console.log("Something went wrong, sorry!");
    }
  });
