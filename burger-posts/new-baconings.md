---
title: New Bacon-ings
date: "2020-01-10"
tags: ["bacon"]
draft: false
summary: A classic bacon-cheeseburger.
images: ["001_new-baconings.png"]
authors: ["steve"]
layout: DEFAULT
---

The burger that started it all. It was a new beginning. No! 

It was a new _bacon-ing_.
