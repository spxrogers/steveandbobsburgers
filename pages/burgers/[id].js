import React from "react";
import { Layout } from "../../components/Layout";
import { getAllPostIds, getPostData } from "../../lib/posts";
import Head from "next/head";
import { Date } from "../../components/Date";
import utilStyles from "../../styles/utils.module.css";
import Image from "next/image";

const Post = ({ postData }) => {
  // eslint-disable-next-line @typescript-eslint/no-var-requires
  const burgerImg = require("../../public/burgers/" + postData.images[0]);

  return (
    <Layout>
      <Head>
        <title>{postData.title}</title>
      </Head>
      <article>
        <h1 className={utilStyles.headingXl}>{postData.title}</h1>
        <div className={utilStyles.lightText}>
          <Date dateString={postData.date} />
        </div>
        <div dangerouslySetInnerHTML={{ __html: postData.contentHtml }} />
      </article>
      <div>
        <Image priority src={burgerImg} alt={postData.img} />
      </div>
    </Layout>
  );
};

export default Post;

export const getStaticPaths = async () => {
  const paths = getAllPostIds();
  return {
    paths,
    fallback: false,
  };
};

export const getStaticProps = async ({ params }) => {
  const postData = await getPostData(params.id);
  return {
    props: {
      postData,
    },
  };
};
