import React from "react";
import { parseISO, format } from "date-fns";

export const Date = ({ dateString }) => (
  <time dateTime={dateString}>
    {format(parseISO(dateString), "LLLL d, yyyy")}
  </time>
);
