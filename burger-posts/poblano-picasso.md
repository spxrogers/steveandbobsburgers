---
title: Poblano Picasso
date: "2022-02-11"
tags: ["ohyea", "salsa"]
draft: false
summary: A beautiful salsa verde topped burger and fresh crunchy veg.
images: ["006_poblano-picasso.jpg"]
authors: ["steve"]
layout: DEFAULT
---

This burger is light, fresh, and SPICY. 

Make this burger to celebrate a promotion, an anniversary, your grandma. Make this burger!

